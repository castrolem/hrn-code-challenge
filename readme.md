# HRN - Code Challenge

Coding Challenge Create a one page webapp/website using the latest technologies
from the visual mockup provided. This mockup is a microsite  which purpose is to
display ticket offers of the conference, enabling the audience to browse it and
find the best deal that meets their needs. Your goal is to deliver the best user
experience in order to achieve the purpose of this page.

## Setup Process
* Clone locally and cd into folder.
* Make sure you have `node ~8.2.1` and `npm ~5.3.0` installed.
* run `yarn install`, *If you don't have yarn installed, follow these [instructions](https://yarnpkg.com/lang/en/docs/install/)*.
* `npm run start-dev-server` and then go to `http://localhost:8080/` to see the site.
* `npm test` To run all the specs.
