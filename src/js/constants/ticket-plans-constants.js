export const ticketPlans = [
  {
    id: 1,
    benefits: [
      'Cold-pressed poke thundercats brooklyn chillwave chatreuse',
      'Craft beer 3 wolf moon tbh hoodie',
      'YOLO synth hammock',
      'Distilery aesthetic VHS affogato',
      'Chia readymade schlitz brunch yuccie echo park'
    ],
    description: 'Save €300',
    duration: 'Until August 31',
    isPopular: false,
    name: 'Summer Saver',
    price: 1595,
    promoCode: false
  },
  {
    id: 2,
    benefits: [
      'Cold-pressed poke thundercats brooklyn chillwave chatreuse',
      'Craft beer 3 wolf moon tbh hoodie',
      'YOLO synth hammock',
      'Distilery aesthetic VHS affogato',
      'Chia readymade schlitz brunch yuccie echo park'
    ],
    description: '',
    duration: 'Until October',
    isPopular: true,
    name: 'Expo Only',
    price: 299,
    promoCode: false
  },
  {
    id: 3,
    benefits: [
      'Cold-pressed poke thundercats brooklyn chillwave chatreuse',
      'Craft beer 3 wolf moon tbh hoodie',
      'YOLO synth hammock',
      'Distilery aesthetic VHS affogato',
      'Chia readymade schlitz brunch yuccie echo park'
    ],
    description: 'Save €300 from the Summer Saver',
    duration: 'Until October',
    isPopular: false,
    name: 'Public & Governmental sector',
    price: 1295,
    promoCode: false
  },
  {
    id: 4,
    benefits: [
      'Cold-pressed poke thundercats brooklyn chillwave chatreuse',
      'Craft beer 3 wolf moon tbh hoodie',
      'YOLO synth hammock',
      'Distilery aesthetic VHS affogato',
      'Chia readymade schlitz brunch yuccie echo park'
    ],
    description: '',
    duration: '',
    isPopular: false,
    name: 'Standard conference & Expo',
    price: 1595,
    promoCode: true
  }
];
