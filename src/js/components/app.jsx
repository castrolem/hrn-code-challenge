import React from 'react';
import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import {Provider} from 'react-redux';

import Template from './template';
import ErrorPage from './shared/app-error-page';

// Conference
import TicketBooking from './conference/ticket-booking';

export default () => {
  return ( // TODO: Replace hashHistory with browserHistory for prod push
    <Router history={hashHistory}>
      <Route path="/" component={Template}>
        <IndexRoute component={TicketBooking} />
        <Route path="single" component={TicketBooking}/>
        <Route path="groups" component={TicketBooking}/>
        <Route path="investors" component={TicketBooking}/>
        <Route path="startups" component={TicketBooking}/>

        <Route path="*" component={ErrorPage}/>
      </Route>
    </Router>
  )
}
