import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {ticketPlans} from '../../constants/ticket-plans-constants';
import Partners from '../shared/partners';
import PriceBox from './price-box';
import PromoCodeInput from './promo-code-input';
import TimeTable from './time-table';

class TicketBooking extends Component {

  state = {
    displayBenefits: false,
    promoCode: '',
    selectedDay: 'BOTH'
  };

  bookPlan = (plan) => {
    let {promoCode, selectedDay} = this.state;
    alert(`${plan.name} - €${plan.price}${promoCode ? `\nWith Promo Code ${promoCode} for ${selectedDay}` : ''}`)
    this.setState({promoCode: '', selectedDay: 'BOTH'}); // reset
  };

  _toggleBenefits = () => {
    this.setState({displayBenefits: !this.state.displayBenefits});
  };

  _updatePromoCode = (e) => {
    this.setState({promoCode: e.target.value});
  };

  _updateDay = (e) => {
    this.setState({selectedDay: e.target.value});
  };

  _renderPlans = () => {
    return ticketPlans.map((plan) => {
      return (
        <PriceBox benefits={plan.benefits} key={plan.id}
                  duration={plan.duration}
                  displayBenefits={this.state.displayBenefits}
                  isPopular={plan.isPopular}
                  name={plan.name}
                  price={plan.price}
                  txt={plan.description}
                  handler={this.bookPlan}>
          {plan.promoCode && <PromoCodeInput updatePromoCode={this._updatePromoCode} updateSelectedDay={this._updateDay}
                                             promoCode={this.state.promoCode} selectedDay={this.state.selectedDay}/> }
        </PriceBox>
      )
    })
  };

  render() {
    return (
      <div id="ticket--booking">
        <TimeTable currentSchedule={2}/>
        <p className="text-center" style={{maxWidth: '600px', margin: '2rem auto'}}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur ea fugit incidunt molestiae natus
          temporibus. Ab asperiores, atque, blanditiis dicta fugit illo iste itaque modi qui quibusdam quidem recusandae
          rerum? Fashion axe keytar truffaut migas Farm-to-table PBR&amp;B. Drinking vinegar sustainable helvetica.
        </p>
        <div className="booking--plans">
          {this._renderPlans()}
        </div>
        <div className="text-center" style={{marginBottom: '4rem'}}>
          <div className="button outline" role="button" onClick={this._toggleBenefits}>
            {this.state.displayBenefits ? 'Close' : 'Compare Benefits'}
          </div>
        </div>
        <Partners />
      </div>
    );
  }
}

export default TicketBooking;
