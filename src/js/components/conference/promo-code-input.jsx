import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class PromoCodeInput extends Component {
  static propTypes = {
    promoCode: PropTypes.string,
    selectedDay: PropTypes.string,
    updatePromoCode: PropTypes.func,
    updateSelectedDay: PropTypes.func
  };

  render() {
    return (
      <div className="promo--box">
        <small>Do you have a promo code?</small>
        <div className="field--container">
          <input type="text" value={this.props.promoCode} onChange={this.props.updatePromoCode}/>
          <span className="promo-code__text font--primary">Type it here & then pick <span className="base">DAY01/DAY02 OR BOTH</span></span>
        </div>
        {this.props.promoCode.length > 4 && <div className="field--container text-left">
          <label htmlFor="day-one" className="custom-radio">
            <input type="radio" id="day-one" name="promo-code-day" value="DAY01"
                   checked={this.props.selectedDay === 'DAY01'} onChange={this.props.updateSelectedDay}/>
            <span>Day 01</span>
          </label>
          <label htmlFor="day-two" className="custom-radio">
            <input type="radio" id="day-two" name="promo-code-day" value="DAY02"
                   checked={this.props.selectedDay === 'DAY02'} onChange={this.props.updateSelectedDay}/>
            <span>Day 02</span>
          </label>
          <label htmlFor="day-both" className="custom-radio">
            <input type="radio" id="day-both" name="promo-code-day" value="BOTH"
                   checked={this.props.selectedDay === 'BOTH'} onChange={this.props.updateSelectedDay}/>
            <span>Both</span>
          </label>
        </div>}
      </div>
    );
  }
}

export default PromoCodeInput;
