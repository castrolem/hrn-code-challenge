import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';

const TimeTable = ({currentSchedule}) => {
  const selectedTime = (schedule) => {
    return ClassNames('time-table__name', {
      'selected': schedule === currentSchedule
    })
  };

  return (
    <div className="time-table font--primary text-uppercase">
      <div className={selectedTime(1)}>
        <span>Early Bird</span>
        <span className="badge">sold out</span>
      </div>
      <div className="time-table__divider">/</div>

      <div className={selectedTime(2)}>
        <span>Summer Saver</span>
        <span className="time-table__clock">
          <i className="fa fa-clock-o black" aria-hidden="true" />
          <span>
            <span className="black">until</span>
            <span className="accent no-margin--left">Aug.31</span>
          </span>
        </span>
      </div>
      <div className="time-table__divider">/</div>

      <div className={selectedTime(3)}>
        <span>Regular</span>
      </div>
      <div className="time-table__divider">/</div>

      <div className={selectedTime(4)}>
        <span>Late</span>
      </div>
      <div className="time-table__divider">/</div>

      <div className={selectedTime(5)}>
        <span>Onsite Registration</span>
      </div>
    </div>
  );
};

TimeTable.propTypes = {
  currentSchedule: PropTypes.number.isRequired
};

export default TimeTable;
