import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const PriceBox = ({benefits, duration, displayBenefits, isPopular, name, price, txt, handler, children}) => {
  const selectButtonClassNames = () => {
    return classNames('button no-margin', {
      'accent': isPopular
    });
  };

  const selectBoxClassNames = () => {
    return classNames('box-layout', {
      'popular': isPopular
    })
  };

  const _renderBenefits = (benefits) => {
    return benefits.map((benefit, i) => <li className="base" key={i}><span className="black">{benefit}</span></li>)
  };

  const bookPlan = () => {
    return handler({name, price})
  };

  return (
    <div className={selectBoxClassNames()}>
      <div className="content text-center">
        {isPopular && <h5 className="accent text-uppercase no-margin">Most Popular</h5>}
        <h5 className="no-margin base text-uppercase">{name}</h5>
        <h2 className="no-margin base">{`€${price}`}</h2>
        {txt && <small>{txt}</small>}
        <p className="base no-margin--top">{duration}</p>
        {children}
        {
          displayBenefits && <ul className="benefits text-left">
            {_renderBenefits(benefits)}
          </ul>
        }
      </div>
      <footer>
        <button onClick={bookPlan} className={selectButtonClassNames()} type="button" role="button">
          Book Now
        </button>
      </footer>
    </div>
  );
};

PriceBox.propTypes = {
  benefits: PropTypes.arrayOf(PropTypes.string).isRequired,
  children: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  displayBenefits: PropTypes.bool,
  duration: PropTypes.string.isRequired,
  handler: PropTypes.func,
  isPopular: PropTypes.bool,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  txt: PropTypes.string
};

export default PriceBox;
