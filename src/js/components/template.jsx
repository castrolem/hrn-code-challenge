import React from 'react';
import PropTypes from 'prop-types';
import Header from './shared/app-header';
import Footer from './shared/app-footer';

const appLayout = (props) => {
  return (
    <div className="app-template">
      <Header currentPath={props.location.pathname} />
      <div className="container">
        <div className="app--content">
          {props.children}
        </div>
      </div>
      <Footer />
    </div>
  );
};

appLayout.propTypes = {
  props: PropTypes.shape({
    location: PropTypes.object
  })
};

const Template = (props) => appLayout(props);

export default Template;
