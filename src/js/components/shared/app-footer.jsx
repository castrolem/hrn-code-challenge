import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import classNames from 'classnames';
import moment from 'moment';
import AppLogo from './app-logo';

const AppFooter = (props) => {

  const copyrightTimeDisplay = () => moment().format('YYYY');

  return (
    <footer className="main--footer text-center">
      <AppLogo/>

      <nav className="main--footer__navigation">
        <ul className="list-reset inline-list footer--navigation">
          <li>
            <Link className="link white text-uppercase" to="/">HRN</Link>
          </li>
          <li>
            <Link className="link white text-uppercase" to="/">About</Link>
          </li>
          <li>
            <Link className="link white text-uppercase" to="/">Team</Link>
          </li>
          <li>
            <Link className="link white text-uppercase" to="/">Jobs</Link>
          </li>
          <li>
            <Link className="link white text-uppercase" to="/">Contact</Link>
          </li>
        </ul>
        
        <div className="footer--social">
          <ul className="list-reset inline-list">
            <li>
              <Link to="/" className="link white">
                <i className="fa fa-twitter" aria-hidden="true"/>
              </Link>
            </li>
            <li>
              <Link to="/" className="link white">
                <i className="fa fa-linkedin" aria-hidden="true"/>
              </Link>
            </li>
            <li>
              <Link to="/" className="link white">
                <i className="fa fa-facebook" aria-hidden="true"/>
              </Link>
            </li>
            <li>
              <Link to="/" className="link white">
                <i className="fa fa-instagram" aria-hidden="true"/>
              </Link>
            </li>
            <li>
              <Link to="/" className="link white">
                <i className="fa fa-slideshare" aria-hidden="true"/>
              </Link>
            </li>
            <li>
              <Link to="/" className="link white">
                <i className="fa fa-youtube-play" aria-hidden="true"/>
              </Link>
            </li>
          </ul>
        </div>
      </nav>
      <small>
        HRN | <Link className="link white" to="/">Terms &amp; Conditions</Link> | <Link className="link white" to="/">Cookie Policy</Link> | Copyright &copy; {copyrightTimeDisplay()} HRN Europe. All Rights Reserved.
      </small>
    </footer>
  );
};

AppFooter.propTypes = {};

export default AppFooter;
