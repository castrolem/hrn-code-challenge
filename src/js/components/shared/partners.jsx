import React from 'react';

const Partners = () => {
  return(
    <section className="app--partners">
      <h6 className="text-uppercase text-center grayed">
        Our trusted partners
      </h6>
      <div className="partners">
        <img src="public/img/workday-logo.png" alt="partner-logo"/>
        <img src="public/img/workday-logo.png" alt="partner-logo"/>
        <img src="public/img/workday-logo.png" alt="partner-logo"/>
        <img src="public/img/workday-logo.png" alt="partner-logo"/>
        <img src="public/img/workday-logo.png" alt="partner-logo"/>
      </div>
    </section>
  );
};

export default Partners;
