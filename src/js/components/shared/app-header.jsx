import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import classNames from 'classnames';
import AppLogo from './app-logo';

class AppHeader extends Component {
  static propTypes = {
    currentPath: PropTypes.string
  };

  state = {
    toggledMenu: true
  };

  setLinkClassNames = (path) => {
    return classNames('link text-uppercase', {
      grayed: !(this.props.currentPath === '/' && path === '/single') && path !== this.props.currentPath
    })
  };

  setMenuClassNames = () => {
    return classNames("list-reset inline-list header--navigation", {
      toggled: this.state.toggledMenu
    })
  };

  toggleMenu = (e) => {
    e.preventDefault();
    this.setState({toggledMenu: !this.state.toggledMenu});
  };

  render() {
    return (
      <header className="main--header">
        <div className="main--header__top">
          <div>
            <AppLogo withSubTxt={true}/>
          </div>

          <div className="menu--toggle">
            <button className="button outline white" style={{border: 0}} role="button" onClick={this.toggleMenu}>
              <i className="fa fa-bars white" aria-hidden="true"/>
            </button>
          </div>
        </div>
        <nav className="main--header__navigation text-center">
          <ul className={this.setMenuClassNames()}>
            <li>
              <Link className={this.setLinkClassNames('/single')} to="/single">
                <i className="fa fa-user" aria-hidden="true"/>
                <span>Single Attendee</span>
              </Link>
            </li>
            <li>
              <Link className={this.setLinkClassNames('/groups')} to="/groups">
                <i className="fa fa-users" aria-hidden="true"/>
                <span>Group Tickets</span>
              </Link>
            </li>
            <li>
              <Link className={this.setLinkClassNames('/investors')} to="/investors">
                <i className="fa fa-bullseye" aria-hidden="true"/>
                <span>Investors</span>
              </Link>
            </li>
            <li>
              <Link className={this.setLinkClassNames('/startups')} to="/startups">
                <i className="fa fa-lightbulb-o" aria-hidden="true"/>
                <span>Startups</span>
              </Link>
            </li>
          </ul>
        </nav>
      </header>
    );
  }
}

export default AppHeader;
