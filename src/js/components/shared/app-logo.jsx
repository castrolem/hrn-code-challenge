import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';

const AppLogo = ({withSubTxt}) => {
  return(
    <div className="app--logo">
      <img src="public/img/hrtechworld-logo.png" alt="HRTECHWORLD"/>
      {withSubTxt && <small className="text-uppercase"><b className="base">Amsterdam</b> 24-25 October 2017</small>}
    </div>
  );
};

AppLogo.propTypes = {
  withSubTxt: PropTypes.bool
};

export default AppLogo;
